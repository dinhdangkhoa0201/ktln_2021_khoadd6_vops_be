package com.kltn.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ktln2021BackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ktln2021BackendApplication.class, args);
    }

}
